import React from "react";
import LayananNiaga from '../homepage/LayananNiaga.js'
import PrioritasNiaga from '../homepage/PrioritasNiaga.js'
import BiayaNiaga from '../homepage/BiayaNiaga.js'
import KlienNiaga from '../homepage/KlienNiaga.js'
import PaketHosting from '../homepage/PaketHosting.js'
import PelangganNiaga from '../homepage/PelangganNiaga.js'
import HeaderHome from '../layout/HeaderHome.js'
import FooterTop from '../layout/FooterTop.js'
import GaransiNiaga from '../homepage/GaransiNiaga.js'
import PembayaranNiaga from '../homepage/PembayaranNiaga.js'
import '../styles/HomePage.css'

const MainPage = () => {
return (
    <div className="App">
        <HeaderHome/>
        <LayananNiaga/>
        <PrioritasNiaga/>  
        <BiayaNiaga/>
        <PelangganNiaga/>
        <PaketHosting/>
        <KlienNiaga/>
        <GaransiNiaga/>
        <PembayaranNiaga/>
        <FooterTop/>
  </div>
  );
}

export default MainPage;
