import React from "react";
import '../styles/HomePage.css'
import { Link } from "react-router-dom"

const HeaderTop = () =>{
  return(
    <div className="header">
       <div className="bar-sticky" >
           <div className="container">
                <div className="bar" >
                    <ul className="up-bar">
                        <li><i className="fa fa-phone"></i></li>
                        <li>0274-2885822</li>
                        <li><i className="fa fa-comment-alt"></i></li>
                        <li>Live Chat</li>
                        <li><i className="fa fa-shopping-cart"></i></li>
                    </ul>
                    <div className="navbar">
                        <a className="logo"><Link to="/">
                            <img className="logo" src={require('../images/nh-logo.png')} alt/>></Link>
                        </a>
                       <ul>
                           <li className="nav-text"><Link to="/unlimited-hosting">UNLIMITED HOSTING</Link></li>
                           <li className="nav-text"><Link to="/cloud-hosting">CLOUD HOSTING</Link></li>
                           <li className="nav-text"><Link to="/cloud-vps">CLOUD VPS</Link></li>
                           <li className="nav-text"><Link to="/domain">DOMAIN</Link></li>
                           <li className="nav-text"><Link to="/afiliasi">AFILIASI</Link></li>
                           <li className="nav-text"><Link to="/blog">BLOG</Link></li>
                           <li></li>
                       </ul>    
                       <button className="btn-blue">LOGIN</button>     
                    </div>
                </div>
               
           </div>
       </div>
    </div>


  )
}

export default HeaderTop

