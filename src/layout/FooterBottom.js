import React from "react";
import '../styles/HomePage.css'

const FooterBottom = () =>{
  return( 
<div className="footer">
    <div className="footer-container">
        <div className="footer-card">
            <h2>Hubungi Kami</h2>
            <ul className="footer-list">
                <li>Telp: 0274-2885822</li>
                <li>WA: 0895422447394</li>
                <li>Senin - Minggu</li>
                <li>24 Jam Non Stop</li>
                <li> - </li>
                <li>Jl. Palagan Tentara Pelajar</li>
                <li>No 81 Jongkang, Sariharjo,</li>
                <li>Ngaglik, Sleman</li>
                <li>Daerah Istimewa Yogyakarta</li>
                <li>55581</li>
            </ul>
        </div>
        <div className="footer-card">
            <h2>Layanan</h2>
            <ul className="footer-list">
                <li>Shared Hosting</li>
                <li>Cloud Hosting</li>
                <li>Cloud VPS Hosting</li>
                <li>Transfer Hosting</li>
                <li>Web Builder</li>
                <li>Keamanan SSL/HTTPS</li>
                <li>Jasa Pembuatan Website</li>
                <li>Program Afiliasi</li>
                <li>Whois</li>
                <li>Niagahoster Status</li>
                <li> Domain </li>
            </ul>
            
        </div>
        <div className="footer-card">
            <h2>Service Hosting</h2>
            <ul className="footer-list">
                <li>Hosting Murah</li>
                <li>Hosting Indonesia</li>
                <li>Hosting Singapore SG</li>
                <li>Hosting Wordpress</li>
                <li>Email Hosting</li>
                <li>Reseller Hosting</li>
                <li>Web Hosting Unlimited</li>
            </ul>
        </div>
        <div className="footer-card">
            <h2>Kenapa Pilih Niagahoster?</h2>
            <ul className="footer-list">
                <li>Hosting Terbaik</li>
                <li>Datacenter Hosting Terbaik</li>
                <li>Domain Gratis</li>
                <li>Bagi-bagi Domain Gratis</li>
                <li>Bagi-bagi Hosting Gratis</li>
                <li>Review Pelanggan</li>
            </ul>
        </div>
        <div className="footer-card">
            <h2>Tutorial</h2>
            <ul className="footer-list">
                <li>Ebook Gratis</li>
                <li>Knowledgebase</li>
                <li>Blog</li>
                <li>Cara Pembayaran</li>
                <li>Niaga Course</li>
            </ul>
        </div>
        <div className="footer-card">
            <h2>Tentang Kami</h2>
            <ul className="footer-list">
                <li>Tentang</li>
                <li>Penawaran & Promo Spesial</li>
                <li>Niaga Poin</li>
                <li>Karir</li>
                <li>Kontak Kami</li>
            </ul>
        </div>
        <div className="footer-card">
            <h2>Newsletter</h2>
            <div className="langganan">
                <input className="newsletter" placeholder="youremail@gmail.com"/>
                <button className="langganan-btn">BERLANGGANAN</button>
            </div>
        </div>
        <div className="footer-card">
            <ul>
                <li className="socmed-icon1"><i className="fab fa-facebook-f"></i></li>
                <li className="socmed-icon2"><i className="fab fa-instagram"></i></li>
                <li className="socmed-icon3"><i className="fab fa-linkedin-in"></i></li>
                <li className="socmed-icon4"><i className="fab fa-twitter"></i></li>
            </ul>
        </div>
    </div>
    <div className="copyright">
        <div className="side1">
            <p>Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, 
                BitNinja and DC DCI-Indonesia. Cloud</p>
            <p>VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
        </div>
        <div className="side2">
            <p>Syarat dan Ketentuan | Kebijakan Privasi</p>
        </div>
    </div>
</div>

)}
export default FooterBottom;

