import React from "react";

const HeaderHome = () =>{
    return (
      <div className="homepage">
        <div className="home1">
          <h1>Unlimited Web Hosting Terbaik di Indonesia</h1>
          <h2>Ada banyak peluang bisa Anda raih dari rumah dengan
            memiliki website. Manfaatkan diskon hosting hingga 75% dan tetap
            produktif di bulan Ramadhan bersama Niagahoster.</h2>
          <h5>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</h5>
          <div className="jam">
            <div>
              <h2>00</h2>
              <p>Hari</p>
            </div>
            <div>
              <h2>00</h2>
              <p>Jam</p>
            </div>
            <div>
              <h2>00</h2>
              <p>Menit</p>
            </div>
            <div>
              <h2>01</h2>
              <p>Detik</p>
            </div>
          </div>
          <div className="titikdua">
            <h6>:</h6>
            <h6>:</h6>
            <h6>:</h6>
          </div>
          <button className="btn-orangehome">PILIH SEKARANG</button>
        </div>
        <div className="home2">
          <div className="img-home">
            <img src={require('../images/home.png')} alt="" />
          </div>
        </div>
      </div>
    );
  }
;

export default HeaderHome;
