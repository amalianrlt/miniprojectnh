import React from 'react';
import Routes from './routes/Routes.js'
import HeaderTop from './layout/HeaderTop.js'
import './styles/HomePage.css'
import FooterBottom from './layout/FooterBottom'
import MainPage from './mainpage/MainPage'
import FooterTop from './layout/FooterTop'


function App() {
  return (
    <div className="App">
      <HeaderTop/>  
      <Routes/>
      <FooterBottom/>
    </div>
  );
}

export default App;
