import React from "react";
import '../styles/HomePage.css'

const PrioritasNiaga = () => {
    return (
    <div className="prioritas">
      <h2>Prioritas Kecepatan dan Keamanan</h2>
      <div className="prioritas-container">
        <div className ="card2-container">
          <div classname="card2">
            <img src={require("../images/prioritas/hosting-super-cepat.svg")}  alt=""/>
            <ul>
              <li><h3>Hosting Super Cepat</h3></li>
              <li><p>Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed Web Server, waktu loading website Anda akan meningkat pesat.</p></li>
            </ul>
          </div>
          <div className="card2">
             <img src={require("../images/prioritas/domain-keamanan-ekstra.svg")}  alt=""/>
            <ul>
              <li><h3>Keamanan Website Ekstra</h3></li>
              <li><p>Teknologi keamanan Imunify 360 memungkinkan website Anda terlindung dari serangan hacker, malware, dan virus berbahaya setiap saat.</p></li>
            </ul>
           </div>
             <button className="btn-orangee">LIHAT SELENGKAPNYA</button>
       </div>
            <div className="img-prioritas">
                 <img className="server" src={require("../images/prioritas/server.webp")}  alt=""/>
                 <img className="imunify" src={require("../images/prioritas/imunify.svg")}  alt=""/>
                 <img className="litespeed" src={require("../images/prioritas/lite-speed.svg")}  alt=""/>
                 <img className="grafik" src={require("../images/prioritas/graphic.svg")}  alt=""/>
            </div>
       </div>
   </div>
    )
}

export default PrioritasNiaga;

