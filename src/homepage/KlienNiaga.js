import React from "react";
import '../styles/HomePage.css'

const KlienNiaga = () => {
    return (
<div className="pelanggan">
    <h2>Dipercaya 52.000+ Pelanggan di Seluruh Indonesia</h2>
    <div className="brand-container">
        <div className="brand"><img src={require("../images/klien/richeese.svg")} alt=""/></div>
        <div className="brand"><img src={require("../images/klien/rabbani.svg")} alt=""/></div>
        <div className="brand"><img src={require("../images/klien/oten.svg")} alt=""/></div>
        <div className="brand"><img src={require("../images/klien/hydro.svg")} alt=""/></div>
        <div className="brand"><img src={require("../images/klien/petro.svg")} alt=""/></div>
    </div>
</div>

)
}

export default KlienNiaga;

