import React from "react";
import '../styles/HomePage.css'



const GaransiNiaga = () => {
  return (

      <div className="garansi">
        <div className="garansi-container">
           <img className="garansi-img" src={require("../images/icong.svg")} alt=""/>
            <ul className="garansi-text">
              <li><h1>Garansi 30 Hari Uang Kembali</h1></li>
              <li><p>Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian.</p></li>
              <li><button className="btn-orange-garansi">MULAI SEKARANG</button></li>
            </ul>
        </div>
      </div>

  )}
export default GaransiNiaga;

