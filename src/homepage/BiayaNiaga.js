import React from "react";
import '../styles/HomePage.css'

const BiayaNiaga = () => {
return(
   <div className="biayahemat">
    <h2>Biaya Hemat, Kualitas Hebat</h2>
    <div className="biayahemat-container">
        <div className="biayaimg">
            <img className ="cowok" src={require("../images/biaya/cowok.png")} alt=""/>
            <img className ="man"  src={require("../images/biaya/man.png")} alt=""/>
            <img className ="woman1"  src={require("../images/biaya/woman1.webp")} alt=""/>
            <img className ="woman2" src={require("../images/biaya/woman2.png")} alt=""/>
            <img className ="intercom" src={require("../images/biaya/i2.svg")} alt=""/>
            <img className ="online" src={require("../images/biaya/i2.svg")} alt=""/>
            
        </div>
           
        <div className ="card3-container">
             <div className="card3">
                 <img src={require("../images/biaya/i1.svg")}  alt=""/>
                 <ul>
                     <li><h3>Harga Murah, Fitur Lengkap</h3></li>
                     <li><p>Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan fitur lengkap,
                          dari auto install WordPress, cPanel lengkap, hingga SSL gratis.</p></li>
                 </ul>
             </div>
             <div className="card3">
                <img src={require("../images/biaya/i3.svg")}  alt=""/>
                <ul>
                    <li><h3>Website Selalu Online</h3></li>
                    <li><p>Jaminan server uptime 99,98% memungkinkan website Anda selalu online 
                        sehingga Anda tidak perlu khawatir kehilangan trafik dan pendapatan.</p></li>
                </ul>
             </div>
             <div className="card3">
                 <img src={require("../images/biaya/i4.svg")}  alt=""/>
                 <ul>
                     <li><h3>Tim Support Andal dan Cepat Tanggap</h3></li>
                     <li><p>Tidak perlu menunggu lama, selesaikan masalah Anda dengan 
                         cepat secara real time melalui live chat 24/7</p></li>
                 </ul>         
             </div>
        </div>
     </div>
  </div>




)
}


export default BiayaNiaga

