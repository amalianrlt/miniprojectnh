import React from "react";
import '../styles/HomePage.css'

class PaketHosting extends React.Component{
  state = {
    paket: [
      {
        id:1,
        label:"Termurah!",
        kategori:"Bayi",
        promo:"Rp 10.000",
        bulan:"/bln",
        description: "Sesuai untuk Pemula atau Belajar Website",
        list: [
          "500MB Disk Space",
          "Unlimited Bandwidth"
        ]
    },
    {
        id:2,
        label:"Diskon up to 34%",
        kategori:"Pelajar",
        harga:"Rp 60.800,-",
        promo: "Rp 40.223",
        bulan:"/bln",
        description: "Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi",
  },
    {
        id:1,
        label:"Diskon up to 75%",
        kategori:"Personal",
        harga:"Rp 106.250,-",
        promo: "Rp 26.563",
        bulan:"/bln",
        description: "Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll",
     },
     {
        id:1,
        label:"Diskon up to 42%",
        kategori:"Bisnis",
        harga:"Rp 147.800,-",
        promo: "Rp 85.724",
        bulan:"/bln",
        description: "Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll",
   }

]}
render(){
  return(
  <div> { this.state.paket } </div>
  )
}
}
export default PaketHosting;



/*const PaketHosting = () => {
    return (
      <div className="paket">
        <h2 className="heading">Pilih Paket Hosting Anda</h2>
        <div className="paket-container">
          <div className="card5">
            <h1 className="title" style={{backgroundColor: '#00bfa5'}}>Termurah!</h1>
            <h1 className="type">Bayi</h1>
            <h2 className="discount" style={{visibility: 'hidden'}}>Rp. 106.250,-</h2>
            <h1 className="harga">Rp 10.000<span style={{fontSize: '15px', color: '#3A4166'}}>/bln</span></h1>
            <button className="btn-orange2">PILIH SEKARANG</button>
            <p className="deskripsi">Sesuai untuk Pemula atau Belajar Website</p>
            <ul className="keuntungan">
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> 500 MB Disk Space</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Bandwidth</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Database</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> 1 Domain</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Instant Backup</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited SSL Gratis Selamanya</li>
            </ul>
            <h5>Lihat detail fitur</h5>
          </div> 
          <div className="card5">
            <h1 className="title" style={{backgroundColor: '#00bfa5'}}>Diskon up to 34%</h1>
            <h1 className="type">Pelajar</h1>
            <h2 className="discount" style={{textDecoration: 'line-through'}}>  Rp. 60.800,-</h2>
            <h1 className="harga">Rp 40.223<span style={{fontSize: '15px', color: '#3A4166'}}>/bln</span></h1>
            <button className="btn-orange2">PILIH SEKARANG</button>
            <p className="deskripsi">Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi</p>
            <ul className="keuntungan">
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Disk Space</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Bandwidth</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited POP3 Email</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Database</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> 10 Addon Domain</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Instant Backup</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Domain Gratis</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited SSL Gratis Selamanya</li>
            </ul>
            <h5>Lihat detail fitur</h5>
          </div> 
          <div className="card5">
            <h1 className="title" style={{backgroundColor: '#FB8C00'}}>Diskon up to 75%</h1>
            <h1 className="type">Personal</h1>
            <h2 className="discount" style={{textDecoration: 'line-through'}}>Rp. 106.250,-</h2>
            <h1 className="harga">Rp 26.563<span style={{fontSize: '15px', color: '#3A4166'}}>/bln</span></h1>
            <button className="btn-orange2">PILIH SEKARANG</button>
            <p className="deskripsi">Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll</p>
            <ul className="keuntungan">
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Disk Space</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Bandwidth</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited POP3 Email</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Database</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Addon Domain</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Instant Backup</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Domain Gratis</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited SSL Gratis Selamanya</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> SpamAsassin Mail Protection</li>
            </ul>
            <h5>Lihat detail fitur</h5>
          </div> 
          <div className="card5">
            <h1 className="title" style={{backgroundColor: '#00bfa5'}}>Diskon up to 42%</h1>
            <h1 className="type">Bisnis</h1>
            <h2 className="discount" style={{textDecoration: 'line-through'}}>Rp. 147.800,-</h2>
            <h1 className="harga">Rp. 85.724<span style={{fontSize: '15px', color: '#3A4166'}}>/bln</span></h1>
            <button className="btn-orange2">PILIH SEKARANG</button>
            <p className="deskripsi">Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll</p>
            <ul className="keuntungan"> 
              <li> Disk Space</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Bandwidth</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited POP3 Email</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Database</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited Addon Domain</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Magic Auto Backup &amp; Restore</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Domain Gratis</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Unlimited SSL Gratis Selamanya</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> Prioritas Layanan Support</li>
              <li><img src="{require('../images/icon-question-mark.svg')}" alt="" /> SpamAsassin Mail Protection</li>
            </ul>
            <h5>Lihat detail fitur</h5>
          </div> 
        </div>
      </div>
    )
}
export default PaketHosting;
*/
