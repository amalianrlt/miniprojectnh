import React from "react";
import '../styles/HomePage.css'

class KataPelanggan extends React.Component{
  state = {
    pelanggan: [
      {
        id: 1,
        image : require("../images/pelanggan/devjavu.png"),
        description : "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
        name : "Didik & Johan - Owner Devjavu",
      },
      {
        id: 1,
        image : require("../images/pelanggan/devjavu.png"),
        description : "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
        name : "Didik & Johan - Owner Devjavu",
      },
      {
        id: 1,
        image : require("../images/pelanggan/devjavu.png"),
        description : "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
        name : "Didik & Johan - Owner Devjavu",
      }
    ]
  }
}

const PelangganNiaga = (props) => {
  let pelanggan = props.dataPelanggan.map (item =>
    <div className="katapelanggan" key={item.id}>
      <div className="card4"> { item.image } </div>
      <p> {item.description} </p>
      <h3> {item.name} </h3>
    </div>
  )

  return(
    <div> {pelanggan} </div>

  )
}

export default KataPelanggan

/*const PelangganNiaga = () => {
    return (
<div className="katapelanggan">
    <h2>Kata Pelanggan Tentang Niagahoster</h2>
    <div className="katapelanggan-container">
        <div className="card4">
             <img src={require("../images/pelanggan/devjavu.png")} alt=""/>
             <p>Website itu sangat penting bagi UMKM sebagai sarana promosi untuk
                memenangkan persaingan di era digital</p>
            <h3>Didik & Johan - Owner Devjavu</h3>
        </div>
        <div className="card4">
              <img src={require("../images/pelanggan/optimizer.png")} alt=""/>
      <p>Bagi saya Niagahoster bukan sekedar penyedia hosting,
                melainkan partner bisnis yang bisa dipercaya</p>
            <h3>Bob Setyo - Owner Digital Optimizer Indonesia</h3>
        </div>
        <div className="card4">
               <img src={require("../images/pelanggan/sateratu.png")} alt=""/>          
      <p>Solusi yang diberkan tim support Niagahoster sangat mudah
                dimengerti buat saya yang tidak paham teknis</p>
            <h3>Budi Seputro - Owner Sate Ratu</h3>
        </div>
</div>
</div>
    )
} */


