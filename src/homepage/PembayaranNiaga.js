import React from "react";
import '../styles/HomePage.css'

const PembayaranNiaga = () => {
    return (
<div className="banks">
        <h3>Beragam pilihan cara pembayaran yang mempercepat proses order hosting &amp; domain Anda.
        </h3>
        <div className="line1">
          <div className="bank"><img src={require("../images/bank/bca.svg")} alt="" /></div>
          <div className="bank">< img src={require("../images/bank/mandiri.svg")} alt="" /></div>
          <div className="bank">< img src={require("../images/bank/bni.svg")} alt="" /></div>
          <div className="bank"><img src={require("../images/bank/bri.svg")} alt="" /></div>
          <div className="bank"><img src={require("../images/bank/bii.svg")} alt="" /></div>
          <div className="bank"><img src={require("../images/bank/cimb.svg")} alt="" /></div>
          <div className="bank"><img src={require("../images/bank/alto.svg")} alt="" /></div>
          <div className="bank"><img src={require("../images/bank/atm-bersama.svg")} alt="" /></div>
         <div className="bank"><img src={require("../images/bank/paypal.svg")} alt="" /></div>
		</div>
        <div className="line2">
        <div className="bank"><img src={require("../images/bank/alfamart.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/indomart.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/pegadaian.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/pos.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/ovo.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/gopay.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/visa.svg")} alt="" /></div>
        <div className="bank"><img src={require("../images/bank/master.svg")} alt="" /></div>
        </div>
      </div>
    )
}

export default PembayaranNiaga;

