import React from "react";
import '../styles/HomePage.css'

class LayananNiaga extends React.Component{
  state = {
    layanan: [
        {
            id:1,
            image: require("../assets/images/icon-1.svg"),
            name:"Unlimited Hosting",
            description: "Cocok untuk website skala kecil dan menengah",
            tag: "Mulai dari",
            price:"Rp 10.000,-"
        },
        {
            id:2,
            image: require("../assets/images/icons-cloud-hosting.svg"),
            name:"Cloud Hosting",
            description: "Kapasitas resource tinggi, fully managed dan mudah dikelola",
            tag: "Mulai dari",
            price:"Rp 10.000,-"
        },
        {
            id:3,
            image: require("../assets/images/icons-cloud-vps.svg"),
            name:"Cloud VPS",
            description: "Dedicated resource dengan akses root dan konfigurasi mandiri",
            tag: "Mulai dari",
            price:"Rp 104.000,-"
      },
      {
          id:4,
          image: require("../assets/images/icons-domain.svg"),
          name:"Domain",
          description: "Temukan nama domain yang Anda inginkan",
          tag: "Mulai dari",
          price:"Rp 14.000,-"
    }
    ]}
  render (){
    return(
    <div>
      { this.state.layanan }  
    </div>
    )
  }
}



/*const LayananNiaga = () => {
    return (
<div className="layanan">
        <h2>Layanan Niagahoster</h2>
        <div className="layanan-container">
          <div className="card1">
<img src={require("../images/layanan/icon-1.svg")} alt=""/>
            <h3>Unlimited Hosting</h3>
            <h4>Cocok untuk website skala kecil dan menengah</h4>
            <h5>Mulai dari</h5>
            <p>Rp 10.000,-</p><p>
            </p></div>
          <div className="card1">
 <img src={require("../images/layanan/icons-cloud-hosting.svg")} alt=""/>
            <h3>Cloud Hosting</h3>
            <h4>Kapasitas resource tinggi, fully managed, dan mudah dikelola</h4>
            <h5>Mulai dari</h5>
            <p>Rp 150.000,-</p><p>
            </p></div>
          <div className="card1">
 <img src={require("../images/layanan/icons-cloud-vps.svg")} alt=""/>
            <h3>Cloud VPS</h3>
            <h4>Dedicated resource dengan akses root dan konfigurasi mandri</h4>
            <h5>Mulai dari</h5>
            <p>Rp 104.000,-</p><p>
            </p></div>
          <div className="card1">
 <img src={require("../images/layanan/icons-domain.svg")} alt=""/>
            <h3>Domain</h3>
            <h4>Temukan nama domain yang anda inginkan</h4>
            <h5>Mulai dari</h5>
            <p>Rp 14.000,-</p><p>
            </p></div>
        </div>
        <div className="website">
  <img src={require("../images/layanan/home-pembuatan-website.svg")} alt=""/>
          <ul>
            <li><h3>Pembuatan Website</h3></li>
            <li><p>500 perusahaan lebih percayakan pembuatan websitenya pada kami. <a href>Cek selengkapnya...</a></p></li>
          </ul>            
        </div>
      </div>
    )
}


export default LayananNiaga
*/
