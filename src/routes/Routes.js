import React from 'react';
import MainPage from '../mainpage/MainPage.js'
import '../styles/HomePage.css'
import Afiliasi from '../views/Afiliasi.js'
import Blog from '../views/Blog.js'
import CloudHosting from '../views/CloudHosting.js'
import CloudVps from '../views/CloudVps.js'
import Domain from '../views/Domain.js'
import UnlimitedHosting from '../views/UnlimitedHosting.js'
import { BrowserRouter, Route } from "react-router-dom"

const Routes = () => {
return (
    <div className="App">
        <Route path ="/" exact> <MainPage/>
        </Route>
        <Route path ="/unlimited-hosting" exact> <UnlimitedHosting/> </Route>
        <Route path ="/cloud-hosting" exact> <CloudHosting/> </Route>
        <Route path ="/cloud-vps" exact> <CloudVps/> </Route>
        <Route path ="/domain" exact> <Domain/> </Route>
        <Route path ="/afiliasi" exact> <Afiliasi/> </Route>
        <Route path ="/blog" exact> <Blog/> </Route>
  </div>
  );
}

export default Routes;
